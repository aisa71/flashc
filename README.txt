This is a free C++ library of a fast diffeomorphic image registration algorithm.

The implementation includes geodesic shooting, image matching and atlas building
models for 3D medical images. This code is only for research purpose, and we request
you to cite our research paper if you use it:

Finite-dimensional Lie algebras for fast diffeomorphic image registration.
Miaomiao Zhang, P. Thomas Fletcher. Information Processing in Medical Imaging (IPMI), 2015.